<?php
class Product
{
    private $conn;
    
    // constructor with $db as database connection
    public function __construct($db)
    {
        $this->conn = $db;
	}
	public function skuIsExists($sku)
    {
        try {
            
                $query = $this->conn->prepare("SELECT COUNT(1) FROM product  WHERE sku = :sku;");
                $query->bindParam(":sku", $sku, PDO::PARAM_STR);
				$query->execute();
				$row = $query->fetchColumn();
            return $row>0;
        }
        catch (PDOException $e) {
            print_r($e->getMessage());
            die($e->getMessage());
        }
        
        return false;
    }
    
    public function getProducts()
    {
        // select all query
        $query = $this->conn->prepare("SELECT p.id,p.name,p.price,p.sku,pd.unitValue,ut.unitType  FROM product p 
                    INNER JOIN  productdetail pd ON p.id=pd.productId
                    INNER JOIN  unittypes     ut ON pd.unitTypeId=ut.id ORDER BY p.price");
        
        $results = array();
        try {
            $query->execute();
            
            while ($rows = $query->fetch(PDO::FETCH_ASSOC)) {
                $results[] = $rows;
            }
        }
        catch (PDOException $e) {
            die($e->getMessage());
        }
        
        return $results;
    }
    
    
    public function deleteSelectedProducts($selectedIds)
    {
        try {
            
            for ($i = 0; $i < count($selectedIds); $i++) {
                $query = $this->conn->prepare("
             DELETE FROM productdetail WHERE productId = :id;
             DELETE FROM product WHERE id = :id;");
                
                $query->bindParam(":id", $selectedIds[$i], PDO::PARAM_INT);
                $query->execute();
            }
            return true;
        }
        catch (PDOException $e) {
            print_r($e->getMessage());
            die($e->getMessage());
        }
        
        return false;
    }
    
    public function addProduct($product)
    {
        try {
            
            $query = $this->conn->prepare("INSERT INTO product(sku,name,price) VALUES(:sku,:name,:price)");
            
            
            $query->bindParam(":sku", $product["sku"], PDO::PARAM_STR);
            $query->bindParam(":name", $product["name"], PDO::PARAM_STR);
            $query->bindParam(":price", $product["price"], PDO::PARAM_STR);
            $query->execute();
            
            $last_id = $this->conn->lastInsertId();
            
            $query = $this->conn->prepare("INSERT INTO productdetail(productId,unitTypeId,unitValue)
          VALUES(:productId,:unitTypeId,:unitValue)");
            
            $query->bindParam(":productId", $last_id, PDO::PARAM_INT);
            $query->bindParam(":unitTypeId", $product["unitTypeId"], PDO::PARAM_INT);
            $query->bindParam(":unitValue", $product["unitValue"], PDO::PARAM_STR);
            $query->execute();
            
            
            return true;
        }
        catch (PDOException $e) {
            print_r($e->getMessage());
            die($e->getMessage());
        }
        
        return false;
    }   
}