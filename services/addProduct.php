<?php
include_once 'includer.php';
include_once '../validation/validateProduct.php';

$postdata = file_get_contents("php://input");
$data     = json_decode($postdata, true);

$validateProduct = new validateProduct();

$productResult = $validateProduct->validateData($data);
//print_r(json_encode($productResult));
if ($productResult->result != 1) {
    print_r(json_encode($productResult));
} else {
   
    $database = new Database();
   
    $db       = $database->getConnection();
    
    $product  = new Product($db);

    $skuIsExists=$product->skuIsExists($data['sku']);

    if($skuIsExists) {
        $productResult->result=false;
        array_push($productResult->message, "Please enter different sku.This sku is exists");
    } else {
        $productResult->result = $product->addProduct($data) ? 1 : 0;
    }
    
    print_r(json_encode($productResult));
}

?>