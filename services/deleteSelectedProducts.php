<?php
header("Access-Control-Allow-Methods:  DELETE");
include_once 'includer.php';

$data = $_GET["id"];

$elements = explode(',', $data);

if(count($elements)==0) print_r(false);
else {
    $database = new Database();
    $db       = $database->getConnection();
    $product = new Product($db);
         
    $result = $product->deleteSelectedProducts($elements);
    print_r(json_encode($result));
}
?>