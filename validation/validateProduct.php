<?php
include_once '../models/returnResult.php';


class validateProduct
{
    
    private $val;
    
    public function __construct()
    {
        $this->val = new returnResult();
    }
    
    
    public function validateData($data)
    {
        
        $sku = trim(stripslashes(htmlspecialchars($data["sku"])));
        
        if (empty($sku)) {
            array_push($this->val->message, "Please enter a valid SKU");
        }
        
        $name = trim(stripslashes(htmlspecialchars($data["name"])));
        
        if (empty($name)) {
            array_push($this->val->message, "Please enter a valid name");
        }
        
        if (intval($data["price"]) == 0) {
            array_push($this->val->message, "Price should greater than 0");
        }
        
        $this->validateUnitValue($data);
        
        $this->val->result = (count($this->val->message) == 0) ? 1 : 0;
        
        return $this->val;
        
    }
    
    function validateUnitValue($data)
    {
        switch ($data["unitTypeId"]) {
            case 3: {
                $dimensionArray = explode('x', $data["unitValue"]);
                
                for ($i = 0; $i < count($dimensionArray); $i++) {
                    
                    if (intval($dimensionArray[$i]) == 0) {
                        array_push($this->val->message, "All Values of dimension should greater than 0");
                        break;
                    }
                }
                break;
            }
            default: {
                if (intval($data["unitValue"]) == 0) {
                    switch ($data["unitTypeId"]) {
                        case 1: {
                            array_push($this->val->message, "Weight should greater than 0");
                            break;
                        }
                        
                        case 2: {
                            array_push($this->val->message, "Size should greater than 0");
                            break;
                        }
                        
                        default:
                            break;
                    }
                }
                break;
            }
        }
    }  
}
?>